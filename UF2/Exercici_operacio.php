<?php
abstract class Operacio{
    protected $valor1;
    protected $valor2;
    protected $resultat;

    public function carregar1($valor1){
        $this->valor1 = $valor1;
    }

    public function carregar2($valor2){
        $this->valor2 = $valor2;
    }

    abstract public function getResultat();
}

class Suma extends Operacio{
    public function Suma($valor1, $valor2){
        $this->carregar1($valor1);
        $this->carregar2($valor2);
    }

    public function getResultat(){
        $this->resultat = $this->valor1 + $this->valor2;
        echo 'El resultado de la suma de '.$this->valor1.' + '.$this->valor2.' es: '.$this->resultat."\n";
    }
}

class Resta extends Operacio{
    public function Resta($valor1, $valor2){
        $this->carregar1($valor1);
        $this->carregar2($valor2);
    }

    public function getResultat(){
        $this->resultat = $this->valor1 - $this->valor2;
        echo 'El resultado de la diferencia de '.$this->valor1.' - '.$this->valor2.' es: '.$this->resultat."\n";
    }
}

$suma = new Suma(10, 10);
$suma->getResultat();

$resta = new Resta(10, 5);
$resta->getResultat();
?>