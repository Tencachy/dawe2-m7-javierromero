<?php
class Impresora{
    public $marca;
    public $model;
    public $numero_de_serie;
    public $connexio;

    public function Impresora($marca, $model, $numero_de_serie, $connexio){
        $this->marca = $marca;
        $this->model = $model;
        $this->numero_de_serie = $numero_de_serie;
        $this->connexio = $connexio;
    }

    public function getImpresoraInfo(){
        return $this->marca.' '.$this->model.' '.$this->numero_de_serie.' '.$this->connexio;
    }
}

$impresoraEPSON = new Impresora('EPSON', 'MX611dhe', '123456', 'USB');
echo $impresoraEPSON->getImpresoraInfo();
?>