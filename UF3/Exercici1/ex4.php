<?php
$conn = mysqli_connect('localhost', 'root', '12345678');
mysqli_select_db($conn, 'ufs');

$consulta = "SELECT nom, cognoms FROM alumnes;";
$resultat = mysqli_query($conn, $consulta);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ex 4 Javier Romero</title>
    <style>
        table,
        td {
            border: 1px solid black;
            border-spacing: 0px;
        }
    </style>
</head>

<body>
    <table>
        <!-- la capçalera de la taula l'hem de fer nosaltres -->
        <thead>
            <td colspan="2" align="center" bgcolor="orange">Nom dels alumnes</td>
        </thead>
        <tr>
            <td>Nom</td>
            <td>Cognoms</td>
        </tr>
        <?php
        while ($registre = mysqli_fetch_assoc($resultat)) {
            echo "\t<tr>\n";
            echo "\t\t<td>" . $registre["nom"] . "</td>\n";
            echo "\t\t<td>" . $registre['cognoms'] . "</td>\n";
            echo "\t</tr>\n";
        }
        ?>
    </table>
</body>

</html>