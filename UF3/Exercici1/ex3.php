<?php
$conn = mysqli_connect('localhost', 'root', '12345678');
mysqli_select_db($conn, 'ufs');

$consulta = "SELECT * FROM assignatures WHERE id_titulacio = (SELECT id_titulacio FROM titulacio WHERE titulacio like 'ESO');";
$resultat = mysqli_query($conn, $consulta);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ex 3 Javier Romero</title>
    <style>
        table,
        td {
            border: 1px solid black;
            border-spacing: 0px;
        }
    </style>
</head>

<body>
    <table>
        <!-- la capçalera de la taula l'hem de fer nosaltres -->
        <thead>
            <td colspan="8" align="center" bgcolor="orange">Assignatures de la ESO</td>
        </thead>
        <tr>
            <td>Id assignatura</td>
            <td>Codi assignatura</td>
            <td>Nom</td>
            <td>Hores</td>
            <td>Descripcio</td>
            <td>Id professor</td>
            <td>Id titulacio</td>
            <td>Curs</td>
        </tr>
        <?php
        while ($registre = mysqli_fetch_assoc($resultat)) {
            echo "\t<tr>\n";
            echo "\t\t<td>" . $registre["id_assignatura"] . "</td>\n";
            echo "\t\t<td>" . $registre['codi_assignatura'] . "</td>\n";
            echo "\t\t<td>" . $registre["nom"] . "</td>\n";
            echo "\t\t<td>" . $registre['hores'] . "</td>\n";
            echo "\t\t<td>" . $registre['descripcio'] . "</td>\n";
            echo "\t\t<td>" . $registre['id_professor'] . "</td>\n";
            echo "\t\t<td>" . $registre['id_titulacio'] . "</td>\n";
            echo "\t\t<td>" . $registre['curs'] . "</td>\n";
            echo "\t</tr>\n";
        }
        ?>
    </table>
</body>

</html>