<?php
$countrycode = $_GET['countrycode'];

$conn = mysqli_connect('localhost', 'root', '12345678');
mysqli_select_db($conn, 'world');
?>

<html>

<head>
    <title>Exemple de lectura de dades a MySQL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        table,
        td {
            border: 1px solid black;
            border-spacing: 0px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1 class="mt-4">Ciudades de
            <?php
            $consulta_pais = "SELECT Name FROM country WHERE Code = '$countrycode';";
            $resultat_pais = mysqli_query($conn, $consulta_pais);
            $registre = mysqli_fetch_assoc($resultat_pais);
            echo $registre["Name"];
            ?>
        </h1>

        <?php
        $consulta = "SELECT * FROM city WHERE CountryCode = '$countrycode';";
        $resultat = mysqli_query($conn, $consulta);

        if (!$resultat) {
            $message  = 'Consulta invàlida: ' . mysqli_error($conn) . "\n";
            $message .= 'Consulta realitzada: ' . $consulta;
            die($message);
        }
        ?>

        <table>
            <!-- la capçalera de la taula l'hem de fer nosaltres -->
            <thead>
                <td colspan="4" align="center" bgcolor="cyan">Llistat de ciutats</td>
            </thead>
            <?php
            # (3.2) Bucle while
            while ($registre = mysqli_fetch_assoc($resultat)) {
                # els \t (tabulador) i els \n (salt de línia) son perquè el codi font quedi llegible

                # (3.3) obrim fila de la taula HTML amb <tr>
                echo "\t<tr>\n";
                echo "\t\t<td>" . $registre["Name"] . "</td>\n";
                echo "\t\t<td>" . $registre['CountryCode'] . "</td>\n";
                echo "\t\t<td>" . $registre["District"] . "</td>\n";
                echo "\t\t<td>" . $registre['Population'] . "</td>\n";
                echo "\t</tr>\n";
            }
            ?>
            <!-- (3.6) tanquem la taula -->
        </table>
    </div>
</body>

</html>