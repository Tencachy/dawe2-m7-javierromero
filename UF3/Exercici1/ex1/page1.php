<html>

<head>
    <title>Exemple de lectura de dades a MySQL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        table,
        td {
            border: 1px solid black;
            border-spacing: 0px;
        }
    </style>
</head>

<body>
    <div class="container">
        <?php
        $conn = mysqli_connect('localhost', 'root', '12345678');
        mysqli_select_db($conn, 'world');

        $consulta = "SELECT * FROM country;";
        $resultat = mysqli_query($conn, $consulta);

        if (!$resultat) {
            $message  = 'Consulta invàlida: ' . mysqli_error($conn) . "\n";
            $message .= 'Consulta realitzada: ' . $consulta;
            die($message);
        }
        ?>

        <form action="page2.php" method="get" class="m-5">
            <label for="countrycode" class="form-label">Selecciona un país</label>
            <select name="countrycode" class="form-select" id="countrycode">
                <?php
                # (3.2) Bucle while
                while ($registre = mysqli_fetch_assoc($resultat)) {
                    echo "\t\t<option value=" . $registre["Code"] . ">" . $registre["Name"] . "</option>\n";
                }
                ?>
            </select>
            <div class="row mt-3">
                <input type="submit" value="Ciudades" class="btn btn-primary col-2">
            </div>
        </form>

        <?php
        $consulta_country = "SELECT * FROM country;";
        $resultat_country = mysqli_query($conn, $consulta_country);
        ?>
        <h2 class="mt-5">Añadir una ciudad</h2>
        <form action="page3.php" method="post">
            <label for="countrycode">Selecciona un país</label>
            <select name="countrycode" class="form-select" id="countrycode">
                <?php
                while ($registre = mysqli_fetch_assoc($resultat_country)) {
                    echo "\t\t<option value=" . $registre["Code"] . ">" . $registre["Name"] . "</option>\n";
                }
                ?>
            </select>

            <label for="cityname" class="mt-3">Nombre de la ciudad</label>
            <input type="text" name="cityname" id="cityname" class="form-control">

            <label for="district" class="mt-3">Nombre del distrito</label>
            <input type="text" name="district" id="district" class="form-control">

            <label for="population" class="mt-3">Población</label>
            <input type="number" name="population" id="population" class="form-control">

            <div class="row mt-3">
                <input type="submit" value="Añadir ciudad" class="btn btn-primary col-2">
            </div>
        </form>
    </div>
</body>

</html>