<?php
try {
    $hostname = "localhost";
    $dbname = "acces_dades";
    $username = "u_acces_dades";
    $pw = "password";
    $pdo = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
} catch (PDOException $e) {
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
}

$query = $pdo->prepare("SELECT i, a FROM prova");
$query->execute();

$row = $query->fetch();
while ($row) {
    echo $row['i']. " - " .$row['a']. "<br>";
    $row = $query->fetch();
}

unset($pdo);
unset($query);