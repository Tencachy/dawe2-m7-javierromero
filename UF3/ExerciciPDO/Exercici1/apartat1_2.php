<?php
try {
    $hostname = "localhost";
    $dbname = "acces_dades";
    $username = "u_acces_dades";
    $pw = "password";
    $pdo = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
} catch (PDOException $e) {
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
}

$query = $pdo->prepare("SELECT i, a FROM prova");
$query->execute();

$row = $query->fetchAll();
foreach($row as $col) {
    echo $col['i']. " - " .$col['a']. "<br>";
}

unset($pdo);
unset($query);