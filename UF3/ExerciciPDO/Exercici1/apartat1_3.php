<?php
try {
    $hostname = "localhost";
    $dbname = "gringottsDB";
    $username = "goblin";
    $pw = "password";
    $pdo = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
} catch (PDOException $e) {
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
}

$query = $pdo->prepare("SELECT goblin_name, last_login FROM goblins");
$query->execute();

$row = $query->fetchAll();
foreach($row as $data) {
    echo $data['goblin_name']. " - " .$data['last_login']. "<br>";
}

unset($pdo);
unset($query);