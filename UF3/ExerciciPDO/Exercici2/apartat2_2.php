<?php 

  try {
    $hostname = "localhost";
    $dbname = "acces_dades";
    $username = "u_acces_dades";
    $pw = "password";
    $dbh = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
  } catch (PDOException $e) {
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
  }
  
  try {
    echo "Comença la inserció<br>";
    //cadascun d'aquests interrogants serà substituit per un paràmetre.
    $stmt = $dbh->prepare("INSERT INTO prova (i, a) VALUES(?,?)"); 
    //a l'execució de la sentència li passem els paràmetres amb un array 
    $stmt->execute( array('13', 'caco')); 
    echo "Insertat!"; 
  } catch(PDOException $e) { 
    print "Error!: " . $e->getMessage() . " Desfem</br>"; 
  } 
   
?> 
