<?php
try {
    $hostname = "localhost";
    $dbname = "gringottsDB";
    $username = "goblin";
    $pw = "password";
    $con = new PDO ("mysql:host=$hostname;dbname=$dbname","$username","$pw");
} catch (PDOException $e) {
    echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    exit;
}
?>