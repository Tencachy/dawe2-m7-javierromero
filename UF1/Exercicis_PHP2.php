<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>

    <body>
        <?php
            //Exercici 1
            echo "1.-<br>";
            function mostrarText() {
                echo "<b>El text que volem mostrar és el següent:</b> 
                M'agraden molt les funcions en <b>PHP</b>";
            }
            mostrarText();
            echo "<br><br>";

            //Exercici 2
            echo "2.-<br>";
            function elevarDosNombres($num1, $num2){
                return pow($num1, $num2);
            }
            $num1 = 2;
            $num2 = 3;
            echo "El resultat de $num1 elevat a $num2 és: " . elevarDosNombres($num1, $num2);
            echo "<br><br>";

            //Exercici 3
            echo "3.-<br>";
            function calcularAreaRectangle($amplada, $alsada){
                $area = $amplada * $alsada;
                echo "Un rectangle amb alçada <b>" . $alsada . "</b> i amb amplada <b>" .$amplada. "</b>
                té una àrea de <b>" .$area. "</b>";
            }
            $amplada = 4;
            $alsada = 2;
            calcularAreaRectangle($amplada, $alsada);
            echo "<br><br>";

            //Exercici 4
            echo "4.-<br>";
            function calcularAreaRectangle2($amplada, $alsada){
                $area = $amplada * $alsada;
                return $area;
            }
            $amplada = 4;
            $alsada = 2;
            echo "Un rectangle amb alçada <b>" . $alsada . "</b> i amb amplada <b>" .$amplada. "</b>
             té una àrea de <b>" .calcularAreaRectangle2($alsada, $amplada). "</b>";
            echo "<br><br>";

            //Exercici 5
            echo "5.-<br>";
            function nombresRecursius($num){
                if($num == 1){
                    echo "El número és $num";
                }else{
                    echo "El número és $num <br>";
                    nombresRecursius($num -1);
                }
            }
            nombresRecursius(10);
            echo "<br><br>";

            //Exercici 6
            echo "6.-<br>";
            $array_edats=array("Marta"=>"31", "Raul"=>"41", "Guillem"=>"39", "Anna"=>"40");
            function mostrarArray($array){
                foreach($array as $nom => $edat){
                    echo "L'edat de $nom és: $edat.<br>";
                }
                echo "<br>";
            }
            echo "Array associatiu: Ordre ascendent, ordenat per valor, edat.<br>";
            asort($array_edats);
            mostrarArray($array_edats);

            echo "Array associatiu: Ordre ascendent, ordenat per clau, nom.<br>";
            ksort($array_edats);
            mostrarArray($array_edats);

            echo "Array associatiu: Ordre descendent, ordenat per valor, edat.<br>";
            arsort($array_edats);
            mostrarArray($array_edats);

            echo "Array associatiu: Ordre descendent, ordenat per clau, nom.<br>";
            krsort($array_edats);
            mostrarArray($array_edats);
            echo "<br>";

            //Exercici 7
            echo "7.-<br>";
            function crearFitxerEscriure($nom_fitxer, $text){
                $file = fopen($nom_fitxer, "w");
                fwrite($file, $text);
            }
            crearFitxerEscriure("primer.txt", "Sóc el primer fitxer");
            crearFitxerEscriure("segon.txt", "Sóc el segon fitxer");
            echo "<br><br>";

            //Exercici 8
            echo "8.-<br>";
            $file2 = fopen("segon.txt", "r+");
            $text = fread($file2, filesize("segon.txt"));
            rewind($file2);
            fwrite($file2, "Frase al principi de tot " . $text);

            $file1 = fopen("primer.txt", "a+");
            fwrite($file1, "Frase al final de tot");
            echo "<br><br>";
        ?>
    </body>
</html>