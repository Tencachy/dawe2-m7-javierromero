<?php
$dinar = array('pizza', 'amanida', 'entrepà');
$amanida = array('enciam' => 'amb',
           'tomaquet' => 'sense',
           'pastanaga' => 'amb');

//2.1
echo "El menú del dinar és: <br><br>";
foreach($dinar as $plat){
    echo $plat."<br>";
}
echo "<br><br><br>";

//2.2
echo "Com m'agrada l'amanida: <br><br>";
foreach($amanida as $ingredient=>$quantitat){
    echo $quantitat." ".$ingredient."<br>";
}
echo "<br><br><br>";

//2.3
echo "El meu array amb el més interessant de les ciutats:<br><br>";
$ciutats = array('New York'=>'Estatua de la Llibertat',
            'Sant Petesburg'=>'Hermitage',
            'Paris'=>'Torre Eiffel',
            'Barcelona'=>'ITB');
foreach($ciutats as $ciutat=>$llocInteressant){
    echo "On està ".$llocInteressant."? està a ".$ciutat."<br>";
}

?>