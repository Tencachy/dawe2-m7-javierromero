<?php
if(isset($_GET['paraula']) && !empty($_GET['paraula'])){
    $paraula = $_GET['paraula'];
}

echo "La paraula a analitzar és: <b>".$paraula."</b><br><br>";
echo "--PRIMERA PART: CREA UNA FUNCIÓ QUE COMPTI EL NUMERO DE LLETRES D'UN STRING:<br>";

function getLongitudParaula($paraula){
    $array_paraula = str_split($paraula);
    return sizeof($array_paraula);
}

echo "Quina és la longitud de la cadena <b>".$paraula."</b>? --->: <b>".getLongitudParaula($paraula)."</b><br><br>";

echo "--SEGONA PART: CREA UNA FUNCIÓ QUE RETORNI L'INVERS D'UN STRING:<br>";

function getInversParaula($paraula){
    $array_paraula = str_split($paraula);
    $paraula_invertida = '';
    for($i=sizeof($array_paraula); $i>=0; $i--){
        $paraula_invertida = $paraula_invertida . $paraula[$i];
    }
    return $paraula_invertida;
}

echo "La paraula <b>".$paraula."</b> invertida és --->: <b>".getInversParaula($paraula)."</b>";
?>