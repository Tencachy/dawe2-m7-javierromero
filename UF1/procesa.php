<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>

    <body>  
        <?php

            function rebreDades($informacio){
                if(isset($_POST[$informacio]) && !empty($_POST[$informacio])){
                    $info = $_POST[$informacio];
                    return $info;
                }
            }

            echo "Nom de l'usuari: " . rebreDades('nomcognoms') . "<br>";
            echo "L'email de l'usuari és: " . rebreDades('email') . "<br>";
            echo "El codi postal és: " . rebreDades('codipostal') . "<br>";
            echo "Comentaris de l'usuari: " . rebreDades('comentaris');
        ?>
    </body>
</html>