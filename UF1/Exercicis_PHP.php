<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>

    <body>
        <?php
            //Exercici 1
            echo "1.-<br>";
            $min = 6;
            $max = 14;
            echo "Límit inferior: " .$min. "<br>";
            echo "Límit superior: " .$max. "<br>";
            for($i=$min; $i<=$max; $i++){
                echo "$i ";
            }
            echo "<br><br>";

            //Exercici 2
            echo "2.-<br>";
            $dia = date('D');
            echo "El dia de la setmana és: ";
            switch($dia){
                case "Mon":
                    echo "dilluns";
                    break;
                case "Tue":
                    echo "dimarts";
                    break;
                case "Wed":
                    echo "dimecres";
                    break;
                case "Thu":
                    echo "dijous";
                    break;
                case "Fri":
                    echo "divendres";
                    break;
                case "Sat":
                    echo "dissabte";
                    break;
                case "Sun":
                    echo "diumenge";
                    break;
            }
            echo "<br><br>";

            //Exercici 3
            echo "3.-<br>";
            $dia = date('D');
            if($dia == "Mon"){
                echo "El dia de la setmana és: dilluns<br>";
            }else if($dia == "Tue"){
                echo "El dia de la setmana és: dimarts<br>";
            }else if($dia == "Wed"){
                echo "El dia de la setmana és: dimecres<br>";
            }else if($dia == "Thu"){
                echo "El dia de la setmana és: dijous<br>";
            }else if($dia == "Fri"){
                echo "El dia de la setmana és: divendres<br>";
            }else{
                echo "Cap de setmana";
            }
            echo "<br><br>";

            //Exercici 4
            echo "4.-<br>";
            $total_compra = 33.55;
            $resta;
            if($total_compra < 30){
                echo "<b>Compra més o et cobrarem 30 euros de despeses d'enviament</b>";
            }else if($total_compra >= 30 && $total_compra < 90){
                $resta = 90 - $total_compra;
                echo "Amb només " .$resta. "€ més podràs tenir despeses d'enviament gratis!!";
            }else{
                echo "<b>Molt bé torni aviat</b>";
            }
            echo "<br><br>";

            //Exercici 5
            echo "5.-<br>";
            $arrayProductes = array(
                0=> array('categoria' => 33, 'nom' => 'Sabates lala'),
                1=> array('categoria' => 24, 'nom' => 'Pantalons lolo'),
                2=> array('categoria' => 33, 'nom' => 'Sabates lulu'),
                3=> array('categoria' => 23, 'nom' => 'Samarreta lili')
            );
            $totalproductes = sizeof($arrayProductes);
            $categoria = 33;
            echo "Els productes de la categoria " .$categoria. " són: <br>";
            for($i = 0; $i<$totalproductes; $i++){
                if($categoria == $arrayProductes[$i]['categoria']){
                    echo $arrayProductes[$i]['nom'] ."<br>";
                }
            }
            echo "<br><br>";
        ?>
    </body>
</html>