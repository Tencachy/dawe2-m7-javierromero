CREATE DATABASE practicauf1;

use practicauf1;

CREATE TABLE ToDoList (
    id_tasca int NOT NULL AUTO_INCREMENT,
    categoria_tasca int NOT NULL,
    text_tasca varchar(100) NOT NULL,
    data_tasca date NOT NULL,
    completa_tasca int NOT NULL,
    PRIMARY KEY (id_tasca)
);