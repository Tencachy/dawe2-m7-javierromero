<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="tasques.css">
    <title>ToDoList</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Titillium+Web:wght@400;700&family=Ubuntu:wght@400;700&display=swap" rel="stylesheet"> 
</head>
<body>
    <div id="contenedor">
        <h1>Les meves tasques</h1>
        <form action="insertar_tasca.php" method="get" class="formulari">
            <p>
                <label for="categoria">Categoria</label>
                <select id="categoria" name="categoria">
                    <!--El value és un número per passar un int a la base de dades-->
                    <option value="0">M6 - Entorn client</option>
                    <option value="1">M7 - Entorn servidor</option>
                    <option value="2">M9 - Disseny interficies</option>
                    <option value="3">M14 - Aplicaciones entrons mobils</option>
                </select>
            </p>
            <p>
                <label for="tasca">Tasques</label>
                <textarea type="text" name="tasca" id="tasca" autocomplete="off"></textarea>
            </p>
            <p>
                <label for="data">Data</label>
                <input type="date" name="data" id="data" autocomplete="off">
            </p>

            <input type="submit" value="Insertar tasca">
        </form>
        <?php
            require_once "conexio.php";

            //array amb les opcions de la categoria, quan s'agafa la categoria(int) de la base de dades s'utilitza com a index
            $array_options = ['M6 - Entorn client', 'M7 - Entorn servidor', 'M9 - Disseny interficies', 'M14 - Aplicaciones entrons mobils'];

            //array amb nom de classes per modificar el color de fons de cada tasca segons el mòdul
            $array_colors = ['tasca-m6', 'tasca-m7', 'tasca-m9', 'tasca-m14'];
            
            print("<h2>Tasques per fer</h2>");
            //select que agafa les tasques no completades
            $selectToDo = "SELECT * from ToDoList WHERE completa_tasca=0";
            $resultatToDo = mysqli_query($connexio, $selectToDo);

            //hi ha un input de tipus hidden que recull l'id de la tasca i la passa en el submit
            while($resultat = mysqli_fetch_row($resultatToDo)){
                echo '<form action="actualizar.php" method="get">
                            <section class="'.$array_colors[$resultat[1]].' tasca">
                                <div class="info">
                                    <p><b>'.$array_options[$resultat[1]].'</b></p>
                                    <h3>'.$resultat[2].'</h3>
                                    <br>
                                    <p>Data de lliurament: '.$resultat[3].'</p>
                                </div>    
                                <div>
                                <input type="hidden" id="id-tasca" name="id-tasca" value="'.$resultat[0].'">
                                <input type="submit" value="Tasca acabada" class="tasca-btn">
                                </div>
                            </section>
                        </form>';
            }
        
            
            print("<h2>Tasques realitzades</h2>");
            //select que agafa les tasques completades de la base de dades
            $selectToDo = "SELECT * from ToDoList WHERE completa_tasca=1";
            $resultatDone = mysqli_query($connexio, $selectToDo);

            //hi ha un input de tipus hidden que recull l'id de la tasca i la passa en el submit
            while($resultatAcabat = mysqli_fetch_row($resultatDone)){
                echo '<form action="borrar.php" method="get">
                          <section class="'.$array_colors[$resultatAcabat[1]].' tasca">
                              <div class="info">
                                  <p><b>'.$array_options[$resultatAcabat[1]].'</b></p>
                                  <h3>'.$resultatAcabat[2].'</h3>
                                  <br>
                                  <p>Data de lliurament: '.$resultatAcabat[3].'</p>
                              </div>    
                              <div>
                              <input type="hidden" id="id-tasca" name="id-tasca" value="'.$resultatAcabat[0].'">
                              <input type="submit" value="Borrar" class="tasca-btn">
                              </div>
                          </section>
                      </form>';
            }
        ?>
    </div>
</body>
</html>