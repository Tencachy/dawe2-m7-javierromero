<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>

    <body>
        <?php

            $num1 = 5;
            $num2 = 10;
            $resultat = $num1 + $num2;
            echo "1.- El resultat de la suma de $num1 + $num2 és: $resultat <br><br>";

            $num4 = 4;
            $num12 = 12;
            echo "3.- Del $num4 al $num12 és: ";
            for($i=$num4; $i<=$num12; $i++){
                echo "$i ";
            }
            echo "<br><br>";

            $colors = array("blanca", "verda", "vermella", "blau", "negre");
            echo "4.- L'herba és " .$colors[1]. ", la neu és " .$colors[0]. ", la foscor és " .$colors[4]. " i finalment el cel és " .$colors[3]. "<br><br>";

            $colors2 = array("vermell", "blau", "verd", "groc");
            echo "5a.-";
            foreach($colors2 as $color){
                echo "\n¿T'agrada el " .$color. "?";
            }
            echo "<br><br>";
            echo "5b.-";
            foreach($colors2 as $color){
                echo "<br>¿T'agrada el " .$color. "?";
            }
            echo "<br><br>";

            $triangle = "*";
            echo "6.- La piràmide queda: <br>";
            for($i=0; $i<10; $i++){
                echo $triangle;
                $triangle .= "*";
                echo "<br>";
            }
        ?>
    </body>
</html>